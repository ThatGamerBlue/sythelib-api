package com.sythelib.plugins.sythelibapi.httpserver.controllers;

import com.google.gson.Gson;
import com.sythelib.plugins.sythelibapi.beans.ErrorBean;
import com.sythelib.plugins.sythelibapi.httpserver.ClientThreadWrapper;
import com.sythelib.plugins.sythelibapi.httpserver.Controller;
import com.sythelib.plugins.sythelibapi.httpserver.Route;
import java.util.Map;
import javax.inject.Inject;
import net.runelite.api.Client;
import net.runelite.api.widgets.Widget;

public class WidgetController implements Controller
{
	@Inject
	private Gson gson;
	@Inject
	private Client client;
	@Inject
	private ClientThreadWrapper wrapper;

	@Route("/widget")
	public String getWidget(Map<String, String> params)
	{
		int x, y;
		try
		{
			x = Integer.parseInt(params.getOrDefault("x", "-1"));
			y = Integer.parseInt(params.getOrDefault("y", "-1"));
		}
		catch (NumberFormatException ex)
		{
			return gson.toJson(ErrorBean.from("number format exception parsing "));
		}

		Widget widget = client.getWidget(x, y);

		return gson.toJson(widget.getBounds());
	}
}
